/************************************************************************
 *                       CSCB63 Winter 2022
 *                  Assignment 1 - AVL Trees
 *                  (c) Mustafa Quraish, Jan. 2021
 *
 * This is the file which should be completed and submitted by you
 * for the assignment. Make sure you have read all the comments
 * and understood what exactly you are supposed to do before you
 * begin. A few test cases are provided in `testClosest.c`, which
 * can be run on the command line as follows:
 *
 *  $> gcc testClosest.c -o testClosest
 *  $> ./testClosest [optional testname]  (or .\testClosest.exe if on Windows)
 *
 * I strongly advise that you write more test cases yourself to see
 * if you have expected behaviour, especially on the edge cases for
 * insert(). You are free to make any reasonable design choices for
 * the implementation of the data structure as long as (1) the outputs
 * are consistent with the expected results, and (2) you meet the
 * complexity requirement. Your closestPair() function will only
 * be tested with cases where there are unique solutions.
 *
 * Mark Breakdown (out of 10):
 *  - 0 marks if the code does not pass at least 1 test case.
 *  - If the code passes at least one test case, then:
 *    - Up to 6 marks for successfully passing all the test cases
 *    - Up to 4 marks for meeting the complexity requirements for 
 *        the functions as described in the comments below.
 *
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
//used for INT_MAX
#include <sys/param.h>
//used for rand_tree function, not required for assignment
#include <time.h>

/**
 * This defines the struct(ure) used to define the nodes
 * for the AVL tree we are going to use for this
 * assignment. You need to add some more fields here to be
 * able to complete the functions in order to meet the
 * complexity requirements
 */
typedef struct avl_node {
  // Stores the value (key) of this node
  int value;

  // Pointers to the children
  struct avl_node *left;
  struct avl_node *right;
  struct avl_node *parent;

  // TODO: Add the other fields you need here to complete the assignment!
  //      (Hint: You need at least 1 more field to keep balance)

  int height;
  //balance factor
  int bl_fac;
  //best epsilon -- used for closest-pair
  int best_eps;

  //not actually required, but saves time for closest-pair because no calls to successor/predecessor are needed
  //might help time-complexity
  int max_e;
  int min_e;
	  

} AVLNode;;

void free_avl(AVLNode *root) {
	if (root == NULL) {
		return;
	}
	if (root->left != NULL) {
		AVLNode *left = root->left;
		root->left = NULL;
		return free_avl(left);
	} else if (root->right != NULL) {
		AVLNode *right = root->right;
		root->right = NULL;
		return free_avl(right);
	} else {
		AVLNode *parent = root->parent;
		free(root);
		return free_avl(parent);
	}
}

/*
 ***
 * Getters and setters
 * All run in O(1)
 ***
*/

//returns 0 if a is null;
int get_val(AVLNode *A) {
	if (A != NULL) {
		return A->value;
	} else {
		return 0;
	}
}

//gets the largest element in A's subtrees, Returns -INT_MAX if error
int get_max(AVLNode *A) {
	if (A == NULL) {
		return -INT_MAX;
	}
	return A->max_e;
}

//gets the smallest element in A's subtrees, Returns INT_MAX if error
int get_min(AVLNode *A) {
	if (A == NULL) {
		return INT_MAX;
	}
	return A->min_e;
}

//returns -1 if a is null
int get_height(AVLNode *A) {
	if (A == NULL) {
		return -1;
	} else {
		return A->height;
	}
}

AVLNode *get_placetaker(AVLNode *A, bool clockwise) {
	if (clockwise) {
		return A->left;
	} else {
		return A->right;
	}
}


int get_eps(AVLNode *A) {
	if (A != NULL) {
		return A->best_eps;
	} else {
		return INT_MAX;
	}
}

//returns an element less than A if there is no successor
int get_succ(AVLNode *A) {
	return MAX(A->value-1,get_min(A->right));
}

//returns an element greater than than A if there is no predecessor
int get_pred(AVLNode *A) {
	return MIN(A->value+1,get_max(A->left));
}

//returns balanced if A has no nodes
int get_blfac(AVLNode *A) {
	if (A != NULL) {
		return A->bl_fac;
	} else {
		return 0;
	}
		
}

/*
 ***
 * Recursive updates, called each time a node is re-positioned 
 * All run in O(1)
 ***
*/

void update_bl_fac(AVLNode *A) {
	A->bl_fac = get_height(A->left)-get_height(A->right);
	return;
}


/*
void best_eps(AVLNode *A) {
	if (A == NULL) {
		return;
	}
	int Al = abs(A->value-get_val(A->left));
	int Ar = abs(A->value-get_val(A->right));
	int Leps = get_eps(A->left);
	int Reps = get_eps(A->right);
	int Lmin = MIN(Al,Leps);
	int Rmin = MIN(Ar,Reps);

	if (A->left == NULL && A->right == NULL) {
		A->best_eps = INT_MAX;
	} else if (A->right == NULL) {
		A->best_eps = Lmin;
	} else if (A->left == NULL) {
		A->best_eps = Rmin;
	} else {
		A->best_eps = MIN(Lmin,Rmin);
	}
}*/

void update_height(AVLNode *A) {
	if (A != NULL) {
		A->height = MAX(get_height(A->left)+1,get_height(A->right)+1);
	}
}

void update_best_eps(AVLNode *A) {
	int val = get_val(A);
	int Leps = get_eps(A->left);
	int Reps = get_eps(A->right);
	int succ = get_succ(A);
	int pred = get_pred(A);
	if (succ < val) {
		succ = val-INT_MAX;
	} else if (pred > val) {
		pred = INT_MAX-val;
	}
	A->best_eps = MIN(MIN(Leps,Reps),MIN(succ-val,abs(val-pred)));
}

void update_max_min(AVLNode *A) {
	int val = A->value;
	int smallest = MIN(val,get_min(A->left));
	int biggest = MAX(val,get_max(A->right));
	A->max_e = biggest;
	A->min_e = smallest;
	return;
}

void update(AVLNode *A) {
	if (A == NULL) {
		return;
	}
	update_bl_fac(A);
	update_max_min(A);
	update_height(A);
	update_best_eps(A);
}


/**
 * This function allocates memory for a new node, and initializes it. 
 * The allocation is already completed for you, in case you haven't used C 
 * before. For future assignments this will be up to you!
 * 
 * TODO: Initialize the new fields you have added
 */
AVLNode *newNode(int value) {

  AVLNode *node = calloc(sizeof(AVLNode), 1);
  if (node == NULL) {  // In case there's an error
    return NULL;
  }

  node->value = value;
  node->left = NULL;
  node->right = NULL;
  node->parent = NULL;
  node->best_eps = INT_MAX;


  // Initialize values of the new fields here...
  node->height = 0;
  node->bl_fac = 0;

  return node;
}


bool bad_blfac(AVLNode *A) {
	if (abs(A->bl_fac) == 2) {
		return true;
	} else {
		return false;
	}
}


void single_rot(AVLNode *new_top, AVLNode *old_top, AVLNode *big_papa, bool clockwise) {
		//fix parents
		AVLNode *rl_jump = get_placetaker(new_top,!clockwise);
		new_top->parent = big_papa;
		old_top->parent = new_top;
		if (rl_jump != NULL) {
			rl_jump->parent = old_top;
		}

		//fix children
		if (clockwise) {
			new_top->right = old_top;
			//rl_jump switches from being a right child to a left child
			//fine if rl_jump is null
			old_top->left = rl_jump;
			if (big_papa != NULL) {
				if (big_papa->value > new_top->value) {
					big_papa->left = new_top;
				} else {
					big_papa->right = new_top;
				}
			}
		} else {
			//rl_jump switches from being a left child to a right child
			new_top->left = old_top;
			old_top->right = rl_jump;
			if (big_papa != NULL) {
				if (big_papa->value > new_top->value) {
					big_papa->left = new_top;
				} else {
					big_papa->right = new_top;
				}
			}
		}
		update(old_top);
		update(new_top);
		update(big_papa);
}

AVLNode *balance_r(AVLNode *root, AVLNode *A) {
	if (A == NULL) {
		return root;
	}
	update(A);
	int blfac = get_blfac(A);
	if (bad_blfac(A)) {
		//we need to rotate -- how?
		bool clockwise;
		if (blfac > 0) {
			clockwise = true;
		} else {
			clockwise = false;
		}
		//loses 2 heights
		AVLNode *old_top = A;
		//new_top doesn't gain any height
		AVLNode *new_top = NULL;

		//null if old_top is root
		AVLNode *big_papa = old_top->parent;

		new_top = get_placetaker(old_top,clockwise);
		//case 1: double rotation (sign difference between new/old nodes)
		//fix: two single rotaions
		if (new_top->bl_fac*old_top->bl_fac == -2) {
			//which direction, opposite!
			//about what node? new_top!
			AVLNode *single_new_top = get_placetaker(new_top,!clockwise);
			//void single_rot(AVLNode *new_top, AVLNode *old_top, AVLNode *big_papa, bool clockwise);
			single_rot(single_new_top,new_top,old_top, !clockwise);

			if (bad_blfac(single_new_top)) {
				return balance_r(root,single_new_top->parent);
			} else {
				return balance_r(root,single_new_top);
			}
		} else {
			//case 2: single rotation
			single_rot(new_top,old_top,big_papa,clockwise);
			if (new_top->parent == NULL) {
				return new_top;
			} else {
				A=new_top;
				//climb up the tree to set other heights properly
			}
		}
	}
	//calculate best_eps -- also fixes big_papa if he exists
	update(A);

	return balance_r(root,A->parent);
}

AVLNode *balance(AVLNode *root, AVLNode *x) {
	return balance_r(root,x);
}



/**
 * This function is supposed to insert a new node with the give value into the 
 * tree rooted at `root` (a valid AVL tree, or NULL)
 *
 *  NOTE: `value` is a positive integer in the range 1 - 1,000,000 (inclusive)
 *       The upper bound here only exists to potentially help make the 
 *                implementation of edge cases easier.
 *
 *  TODO:
 *  - Make a node with the value and insert it into the tree
 *  - Make sure the tree is balanced (AVL property is satisfied)
 *  - Return the *head* of the new tree (A balance might change this!)
 *  - Make sure the function runs in O(log(n)) time (n = number of nodes)
 * 
 * If the value is already in the tree, do nothing and just return the root. 
 * You do not need to print an error message.
 *
 * ----
 * 
 * An example call to this function is given below. Note how the
 * caller is responsible for updating the root of the tree:
 *
 *  AVLNod *root = (... some tree is initialized ...);
 *  root = insert(root, 5); // Update the root!
 */
AVLNode *insert_recur(AVLNode *A, AVLNode *x) {
	if (A == NULL) {
		return x;
	} else if (A == x) {
		puts("For some reason root is equal to the node being inserted");
		//abort();
	} else if (A->value == 0) {
		puts("This is impossible, A->value = 0");
		//abort();
	}
	if (x->value < A->value) {
		if (A->left == NULL) {
		    A->left = x;
			x->parent = A;
		} else {
		    return insert_recur(A->left,x);
		}
	} else if (x->value > A->value) {
		if (A->right == NULL) {
		    A->right = x;
			x->parent = A;
		} else {
		    return insert_recur(A->right,x);
		}
	} else {
		return NULL;
	}
	update(A);
	return NULL;
}

AVLNode *insert(AVLNode *root, int value) {
	if (value < 1 || value > 1000000) {
		return root;
	}
	AVLNode *x = newNode(value);
	AVLNode *newroot = NULL;

	newroot = insert_recur(root,x);
	if (root == NULL) {
		root=newroot;
	}
	root = balance(root,x);
	return root;
}

/**
 * This function returns the closest pair of points in the tree rooted
 * at `root`. You can assume there are at least 2 values already in the
 * tree. Since you cannot return multiple values in C, for this function
 * we will be using pointers to return the pair. In particular, you need
 * to set the values for the two closest points in the locations pointed
 * to by `a` and `b`. For example, if the closest pair of points is
 * `10` and `11`, your code should have something like this:
 *
 *   (*a) = 10 // This sets the value at the address `a` to 10
 *   (*b) = 11 // This sets the value at the address `b` to 11
 *
 * NOTE: Make sure `(*a)` stores the smaller of the two values, and
 *                 `(*b)` stores the greater of the two values.
 * 
 * NOTE: The test cases will have a unique solution, don't worry about 
 *        multiple closest pairs here.
 *
 *
 * TODO: Complete this function to return the correct closest pair.
 *       Your function should not be any slower than O(log(n)), but if 
 *       you are smart about it you can do it in constant time.
 */


/*
 * Four cases:
 * 	best_eps is left
 * 	best_eps is right
 * 	best_eps is self & b=succ(A)
 * 	best_eps is self & b=pred(A)
*/
void closest_r(AVLNode *A, int *a, int *b) {
	int best_eps = get_eps(A);

	int Leps = get_eps(A->left);
	int Reps = get_eps(A->right);

	if (Leps == best_eps) {
		return closest_r(A->left,a,b);
	} else if (Reps == best_eps) {
		return closest_r(A->right,a,b);
	} else {
		int val = get_val(A);
		*a = val;
		int succ = get_succ(A);
		int pred = get_pred(A);

		if (succ > val && succ-val == best_eps) {
			*b = succ;
			return;
		} else if (pred < val && val-pred == best_eps) {
			*b = pred;
			return;
		} else {
			printf("no best eps found\n");
			//abort();
		}
	}
}
void closestPair(AVLNode *root, int *a, int *b) {
	//check if A == null for the root node or that there are at least two elements
	if (root == NULL) {
		return;
	}
	int sz = 1;
	if (root->left != NULL) {
		sz++;
	}
	if (root->right != NULL) {
		sz++;
	}
	if (sz < 2) {
		return;
	}

	closest_r(root,a,b);
	if (*a > *b) {
		int g = *a;
		int l = *b;
		*a = l;
		*b = g;
	}
	return;
}

/******************************************************************************
 * QUERY() and DELETE() are not part for this assignment, but I recommend you
 * try to implement them on your own time to make sure you understand how AVL
 * trees work.
 *
 *                              End of Assignment 1
 *****************************************************************************/
AVLNode *query_node(AVLNode *A, int value) {
	if (A == NULL) {
		return NULL;
	}
	if (value < A->value) {
		return query_node(A->left,value);
	} else if (value > A->value) {
		return query_node(A->right,value);
	} else {
		return A;
	}
}
bool query(AVLNode *A,int value) {
	AVLNode *res = query_node(A,value);
	if (res == NULL) {
		return false;
	} else {
		return true;
	}
}
AVLNode *delete(AVLNode *A, int value) {
	return NULL;
}

AVLNode *rand_tree(int n) {
	AVLNode *root = NULL;
	srand(time(0));
	int i = 1;
	while (i < n) {
		int val = abs(rand() % 1000000);
		root = insert(root,val);
		i++;
	}
	return root;
}
