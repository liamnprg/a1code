/******************************************************************************
 *                       CSCB63 Winter 2022
 *                  Assignment 1 - AVL Trees
 *                  (c) Mustafa Quraish, Jan. 2021
 *
 * This code imports and tests your solution to the assignment. This is
 * NOT comprehensive, passing these tests does not mean your program is
 * fully correct. You need to ensure this yourself. 
 * 
 * You are *strongly encouraged* to add more test cases here yourself to
 * verify the correctness of your code, and also share tricky edge cases
 * with others (but NOT your solution).
 * 
 * You can compile and run these tests as follows:
 *
 *  $> gcc testClosest.c -o testClosest
 *  $> ./testClosest [optional testname]  (or .\testClosest.exe if on Windows)
 * 
 *  For instance:
 *     ./testClosest basic_test    will run the first test case
 *     ./testClosest               will runn all the test cases
 *
 *****************************************************************************/

#include "unittest.h"
#include "assert.h"

#include "closest.c" // Imports your code

AVLNode *list_to_avl(int *v,int size) {
	AVLNode *root = NULL;
	int i = 0;
	while (i < size) {
		root = insert(root, v[i]);
		i++;
	}
	return root;
}

void bigger_smaller(AVLNode *root) {
	if (root == NULL) {
		return;
	}
	int val = get_val(root);
	if (root->left != NULL)
		assert(get_val(root->left) < val);
	bigger_smaller(root->left);
	if (root->right != NULL)
		assert(get_val(root->right) > val);
	bigger_smaller(root->right);
}

void check_bl_facs(AVLNode *A) {
	if (A == NULL)
		return;
	check_bl_facs(A->left);
	check_bl_facs(A->right);
	update(A);
	assert(abs(get_blfac(A)) <= 2);
}


void cassert(AVLNode *A, int v) {
	if (A == NULL) {
		return assert(false);
	} else {
		return assert(A->value == v);
	}
}


TEST(in1) {
	AVLNode *root = NULL;
	root = insert(root,4);

	root = insert(root,3);
	root = insert(root,5);

	root = insert(root,2);
	root = insert(root,1);
	//done

	cassert(root,4);
		cassert(root->left,2);
			cassert(root->left->left,1);
			cassert(root->left->right,3);
		cassert(root->right,5);
	bigger_smaller(root);
	free_avl(root);
}

TEST(in2) {
	AVLNode *root = NULL;
	root = insert(root,2);

	root = insert(root,1);
	root = insert(root,3);

	root = insert(root,4);
	root = insert(root,5);

	cassert(root,2);
		cassert(root->left,1);
		cassert(root->right,4);
			cassert(root->right->left,3);
			cassert(root->right->right,5);
	bigger_smaller(root);
	free_avl(root);
}

TEST(in3) {
	AVLNode *root = NULL;
	root = insert(root,2);

	root = insert(root,1);
	root = insert(root,3);

	root = insert(root,5);
	root = insert(root,4);

	cassert(root,2);
		cassert(root->left,1);
		cassert(root->right,4);
			cassert(root->right->left,3);
			cassert(root->right->right,5);
	bigger_smaller(root);
	free_avl(root);
}

TEST(in4) {
	AVLNode *root = NULL;
	root = insert(root,4);

	root = insert(root,3);
	root = insert(root,5);

	root = insert(root,1);
	root = insert(root,2);

	cassert(root,4);
		cassert(root->left,2);
			cassert(root->left->left,1);
			cassert(root->left->right,3);
		cassert(root->right,5);
	bigger_smaller(root);
	free_avl(root);
}

TEST(update1) {
	AVLNode *root = NULL;
	root = insert(root,25);
	root = insert(root,27);
	root = insert(root,20);

	bigger_smaller(root);
	assert(root->best_eps == 2);
	free_avl(root);
}


// Run with ./testClosest basic_test
TEST(basic_test) {
  int a, b;
  AVLNode *root = NULL;
  root = insert(root, 100);
  root = insert(root, 200);

  // Test one, two values in the tree
	bigger_smaller(root);
  closestPair(root, &a, &b);
  printf("%d,%d\n",a,b);
  assert(a == 100);
  assert(b == 200);
	free_avl(root);
}

TEST(updated_pair) {
  int a, b;
  AVLNode *root = NULL;
  root = insert(root, 100);
  root = insert(root, 200);
  root = insert(root, 140); // This is now closest to 100

  closestPair(root, &a, &b);
  assert(a == 100);
  assert(b == 140);
	bigger_smaller(root);
	free_avl(root);
}

TEST(many_inserts) {
  int a, b;
  int vals[] = {100, 200, 140, 130, 195, 154, 102, 155};
  AVLNode *root = NULL;
  for (int i = 0; i < 8; i++)
    root = insert(root, vals[i]);

  closestPair(root, &a, &b);
  assert(a == 154);
  assert(b == 155);
	bigger_smaller(root);
	free_avl(root);
}

TEST(many_ins_2) {
  int a, b;
  int vals[] = {100, 200, 140, 130, 195, 102, 155};
  AVLNode *root = list_to_avl(vals,7);

  closestPair(root, &a, &b);
  assert(a == 100);
  assert(b == 102);
	bigger_smaller(root);
	free_avl(root);
}

TEST(many_ins_3) {
  int a, b;
  int vals[] = {200, 140, 130, 195, 102, 155};
  AVLNode *root = list_to_avl(vals,6);

  closestPair(root, &a, &b);
  assert(a == 195);
  assert(b == 200);
	bigger_smaller(root);
	free_avl(root);
}

TEST(many_ins_4) {
  int a, b;
  int vals[] = {130, 140, 195, 102, 155};
  AVLNode *root = list_to_avl(vals,5);

  closestPair(root, &a, &b);
  assert(a == 130);
  assert(b == 140);
	bigger_smaller(root);
	free_avl(root);
}

TEST(many_ins_5) {
  int a, b;
  int vals[] = {130, 195, 102, 155};
  AVLNode *root = list_to_avl(vals,4);

  closestPair(root, &a, &b);
  assert(a == 130);
  assert(b == 155);
	bigger_smaller(root);
	free_avl(root);
}

TEST(ins_extreme_vals) {
	int vals[] = {-10,-5,2000000,3000000,0,1,1,1,1,1};
	AVLNode *root = list_to_avl(vals,10);

	cassert(root,1);
}

TEST(tree_balanced) {
	int reps = 1000;
	int n = 100;
	int rcount = 1;
	while (rcount < reps) {
		AVLNode *root = rand_tree(n);
		check_bl_facs(root);
		bigger_smaller(root);
		free_avl(root);
		rcount++;
	}
}

int get_cp_eps(int *vals, int size) {
	int a = -1000000;
	int b =  1000000;
	int i = 0;
	int j = 0;
	while (i < size) {
		while (j < size) {
			if ((abs(vals[i]-vals[j]) < abs(b-a)) &&( j != i) && (vals[i] != vals[j])) {
				a = vals[i];
				b = vals[j];
			}
			j++;
		}
		j=0;
		i++;
	}
	return abs(a-b);
}

int *rand_list(int n) {
	int *list = calloc(n,sizeof(int));
	srand(time(0));
	int i = 0;
	while (i < n) {
		int val = MAX(1,abs(rand() % 1000000));
		list[i] = val;
		i++;
	}
	return list;
}

TEST(closest_pair_prop) {
	int n = 1000;
	int size = 1000;
	int i = 0;
	while ( i < n) {
		int *list = rand_list(n);
		AVLNode *root = list_to_avl(list,size);
		int a = 0;
		int b = 0;
		closestPair(root, &a, &b);
		int eps = get_cp_eps(list,size);
		if (b-a != eps) {
			int eps2 = get_cp_eps(list,size);
			printf("b=%d,a=%d,eps=%d\n",b,a,eps);
			assert(false);
		}
		free_avl(root);
		free(list);
		i+=1;
	}
}

TEST(succ_pred) {
	int vals[] = {5,3,7,2,4,6,9};
	AVLNode *root = list_to_avl(vals,7);
	assert(get_min(root) == 2);
	assert(get_max(root) == 9);
	assert(get_pred(root) == 4);
	assert(get_succ(root) == 6);
	assert(get_succ(root->left) == 4);
	assert(get_pred(root->left) == 2);
}


// There will be more test cases here when you are being marked...
// Also, you need to make sure you meet the compelxity bounds! Half
// the marks for this coding section will be for meeting the complexity
// bound and having the right idea for implementation, the other half
// will be for passing all the test-cases.

int main(int argc, char *argv[]) {
  // Call the unit testing framework
  unit_main(argc, argv);
  return 0;
}
