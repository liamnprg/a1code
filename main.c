#include "closest.c"
//
#include <stdbool.h>
#include <string.h>
#include <errno.h>

int print_node(AVLNode *A) {

	if (A->value % 10 == A->value && A->value >=0) {
		printf("0%d",A->value);
	} else {
		printf("%d",A->value);
	}
	return 2;
}

void print_node_summary(AVLNode *A) {
	printf("summary of node %d\n",A->value);
	if (A->left != NULL) {
		printf("\tleft: %d\n",A->left->value);
	} else {
		printf("\tleft: NULL\n");
	}
	if (A->right != NULL) {
		printf("\tright: %d\n",A->right->value);
	} else {
		printf("\tright: NULL\n");
	}
	printf("\theight: %d\n",A->height);
	printf("\tbl_fac: %d\n",A->bl_fac);
	printf("\tbest_eps: %d\n",A->best_eps);

}

int spacesz = 2;

//update to find largest node then space everything evenly
void print_tree_r(AVLNode *A,int spaceno,bool went_right) {
    if (A == NULL) {
	    return;
    }
    int sz = print_node(A);
    if (A->left != NULL) {
		printf("--");
		print_tree_r(A->left,spaceno+spacesz+sz,false);
    }
    if (A->right != NULL) {
		printf("\n");
		for (int i = 0; i<spaceno; i++) {
			if (i % (spacesz+sz) == 0) {
				printf("|");
			} else {
				printf(" ");
			}
		}
		printf("|--");
        print_tree_r(A->right,spaceno+spacesz+sz,true);
    }
}

long unsigned int SIZE = 10;

void print_tree(AVLNode *A) {
	print_tree_r(A,0,false);
	puts("\n");
}

int get_str(char *buf) {
	char *res = fgets(buf,SIZE,stdin);
	if (res == NULL) {
		return -1;
	} else {
		return 0;
	}
}

int get_value(int *value) {
	char *endptr = NULL;
	int retval;
	errno = -1;

	while (errno != 0) {
		char *buf = (char *) calloc(SIZE+1,'\0');
		errno = 0;
		printf("value: ");
		retval = get_str(buf);
		if (retval == -1) {
			return retval;
		}
		retval = (int) strtol(buf,&endptr,10);
		*value = retval;
		free(buf);
	}
	return 0;
}

int main() {
    AVLNode *root = NULL;

	char *buf = (char *) calloc(SIZE+1,'\0');
	int value;
	int errmark = 0;

	while (errmark != -1) {
		printf("input 2-letter function symbol: ");
		errmark = get_str(buf);
		if (strncmp(buf,"in",1) == 0) {
			//insert
			errmark = get_value(&value);
			if (errmark != -1) {
				root = insert(root,value);
			}
			printf("inserted %d\n",value);
		} else if (strncmp(buf,"de",2) == 0) {
			//delete
			errmark = get_value(&value);
			if (errmark != -1) {
				root = delete(root,value);
			}
			printf("deleted %d\n",value);
		} else if (strncmp(buf,"cp",2) == 0) {
			//closest-pair
			int a;
			int b;
			closestPair(root,&a,&b);
			printf("%d,%d\n",a,b);
		} else if (strncmp(buf,"qe",2) == 0) {
			//query
			errmark = get_value(&value);
			bool res = false;
			if (errmark != -1) {
				res = query(root,value);
			}
			res == true ? printf("found %d\n",value) : printf("did not find %d\n",value) ;
		} else if (strncmp(buf,"rt",2) == 0) {
			free_avl(root);
			errmark = get_value(&value);
			bool res = false;
			if (errmark != -1) {
				root = rand_tree(value);
			}
			printf("Generated random tree with %d values\n",value);
		} else if (strncmp(buf,"pt",2) == 0) {
			puts("");
			print_tree(root);
		} else if (strncmp(buf,"pn",2) == 0) {
			errmark = get_value(&value);
			if (errmark != -1) {
				AVLNode *res = query_node(root,value);
				if (res != NULL) {
					print_node_summary(res);
				} else {
					printf("Did not find %d\n",value);
				}
			}
		}
		buf[0] = '\0';
		buf[1] = '\0';
	}
	free_avl(root);
	puts("\nbye");
}
