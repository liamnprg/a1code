# A1Code

This project is a school assignment from University of Toronto @ Scarborough, for the class CSCB63. The assignment involves creating an AVL Tree, then writing a function ClosestPair which finds the two closest integers in the tree in `O(log_n)` time. This required augmenting the tree to store metadata which was used to speed up ClosestPair. 
